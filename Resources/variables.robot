*** Variables ***
#host
${url-host}    http://riderr.20scoopscnx.com/


#setup
${browser}    gc
${speed-test}    3

#capture page
${directory-img}    img/

#user
${email_login}      sniperaek@gmail.com
${password_login}       20scoops
${username}    Aekachai

#login
${btn_login_homepage}    id=headerLogin
${text_email}    id=email
${text_password}    id=password
${btn_login}   id=login
${msg_login_success}    Login Success
${msg_emp_email}    The email field is required.
${msg_emp_pass}     The password field is required.
${msg_wrong_email}     Invalid email or Password
${msg_wrong_pass}     Invalid email or Password

#forgot_password
${link_forgot_password}   id=forgotPassword
${text_forgot_password}    id=email
${btn_send_email}    id=sendEmail
${msg_fail_forgot}    Email not found.
${msg_success_forgot}    Success To Send Email
${msg_emp_forgot}    The email field is required.

