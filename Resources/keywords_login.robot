*** Settings ***
Resource          ../settings.robot
*** Keywords ***
TC01 Normal Case
    open riderr
    Click Element    ${btn_login_homepage}
    Input Text    ${text_email}    ${email_login}
    Input Password    ${text_password}    ${password_login}
    Click Button    ${btn_login}
    Wait Until Page Contains    ${msg_login_success}
    Close Window

TC02 Invalid email Case
    open riderr
    Click Element    ${btn_login_homepage}
    Input Text    ${text_email}    xxxx@20scoops.com
    Input Password    ${text_password}    ${password_login}
    Click Button    ${btn_login}
    Wait Until Page Contains    ${msg_wrong_email}
    Close Window

TC03 Invalid password Case
    open riderr
    Click Element    ${btn_login_homepage}
    Input Text    ${text_email}    ${email_login}
    Input Password    ${text_password}    xxxx
    Click Button    ${btn_login}
    Wait Until Page Contains    ${msg_wrong_pass}
    Close Window

TC04 Emty email Case
    open riderr
    Click Element    ${btn_login_homepage}
    Input Password    ${text_password}    ${password_login}
    Click Button    ${btn_login}
    Wait Until Page Contains    ${msg_emp_email}
    Close Window

TC05 Emty password Case
    open riderr
    Click Element    ${btn_login_homepage}
    Input Text    ${text_email}    ${email_login}
    Click Button    ${btn_login}
    Wait Until Page Contains    ${msg_emp_pass}
    Close Window

TC06 Emty email&password Case
    open riderr
    Click Element    ${btn_login_homepage}
    Click Button    ${btn_login}
    Wait Until Page Contains    ${msg_emp_email}
    Wait Until Page Contains    ${msg_emp_pass}
    Close Window

TC01 Valid email Case
    open riderr
    Click Element    ${btn_login_homepage}
    Click Button    ${btn_forgot_password}
    Input Text    ${text_forgot_password}    xxxx@20scoops.com
    Click Button    ${btn_send_email}
    Wait Until Page Contains    ${msg_succe_forgot}
    Close Window

TC02 Invalid email Case
    open riderr
    Click Element    ${btn_login_homepage}
    Click Button    ${btn_forgot_password}
    Input Text    ${text_forgot_password}    xxxx@20scoops.com
    Click Button    ${btn_send_email}
    Wait Until Page Contains    ${msg_fail_forgot}
    Close Window

TC03 Invalid email Case
    open riderr
    Click Element    ${btn_login_homepage}
    Click Button    ${btn_forgot_password}
    Click Button    ${btn_send_email}
    Wait Until Page Contains    ${msg_emp_forgot}
    Close Window