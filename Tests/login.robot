*** Settings ***
Resource          ../settings.robot
# Test Setup      open riderr
Test Teardown   close all browsers

*** Test Cases ***

Test If Else Condition
   [Documentation]  This is spatarrrrr
   [Tags]  eiei
   run keyword if  ${speed-test} < 4   open riderr
   ...  ELSE  CAPTURE PAGE SCREENSHOT
Test Login
    TC01 Normal Case
    TC02 Invalid email Case
    TC03 Invalid password Case
    TC04 Emty email Case
    TC05 Emty password Case
    TC06 Emty email&password Case
Test Forgot Password
    TC01 Valid email Case
    TC02 Invalid email Case
    TC03 Empty email Case